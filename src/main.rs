#![feature(let_chains)]

#[cfg(test)]
extern crate quickcheck;
#[cfg(test)]
#[macro_use(quickcheck)]
extern crate quickcheck_macros;

mod model;
mod rustdoc;

#[cfg(test)]
mod quickcheck_tests;

#[cfg(test)]
mod tests;

fn convert_token(rst: rustdoc::LangStringToken<'_>) -> model::LangStringToken {
    match rst {
        // stripping '"' out of all strings is probably a bad idea
        rustdoc::LangStringToken::KeyValueAttribute(key, value) => {
            model::LangStringToken::KeyValueAttribute(key.to_owned(), value.to_owned())
        }
        rustdoc::LangStringToken::ClassAttribute(class) => {
            model::LangStringToken::ClassAttribute(class.to_owned())
        }
        rustdoc::LangStringToken::LangToken(token) => {
            model::LangStringToken::LangToken(token.to_owned())
        }
    }
}

fn main() {
    let mut args = std::env::args();
    let exe = args.next().expect("first parameter is executable name");
    let xs = args.next().expect(&format!("usage: {exe} [query]"));

    let mut tag_iter = rustdoc::TagIterator::new(&xs);
    let rustdoc_tokens = (&mut tag_iter).collect::<Vec<_>>();
    let model_error;
    let (model_tokens, rem) = match model::parse_lang_string(&xs) {
        Ok((model_tokens, rem)) => (model_tokens, rem),
        Err(err) => {
            model_error = format!("ERROR: {err}");
            (Vec::new(), &model_error[..])
        }
    };
    let converted_rustdoc_tokens = rustdoc_tokens
        .into_iter()
        .map(convert_token)
        .collect::<Vec<model::LangStringToken>>();

    if rem != "" && tag_iter.errors.len() != 0 {
        // both parsers failed; this is fine
        println!("RUSTDOC and MODEL both failed");
        println!("MODEL ERROR: {rem}");
        let e = tag_iter.errors.join(", ");
        println!("RUSTDOC ERROR: {e}");
    } else if rem == "" && tag_iter.errors.len() != 0 {
        let e = tag_iter.errors.join(", ");
        println!("RUSTDOC parser errored, while MODEL parser passed: {e}");
        println!("MODEL: {model_tokens:#?}");
        println!("RUSTDOC: {converted_rustdoc_tokens:#?}");
    } else if rem != "" && tag_iter.errors.len() == 0 {
        println!("RUSTDOC parser passed, while MODEL parser errored: {rem}");
        println!("MODEL: {model_tokens:#?}");
        println!("RUSTDOC: {converted_rustdoc_tokens:#?}");
    } else if converted_rustdoc_tokens != model_tokens {
        println!("MODEL and RUSTDOC give different results");
        println!("MODEL: {model_tokens:#?}");
        if rem != "" {
            println!("MODEL ERROR: {rem}");
        }
        println!("RUSTDOC: {converted_rustdoc_tokens:#?}");
        if tag_iter.errors.len() != 0 {
            let e = tag_iter.errors.join(", ");
            println!("RUSTDOC ERROR: {e}");
        }
    } else {
        if rem != "" {
            println!("MODEL ERROR: {rem}");
        }
        if tag_iter.errors.len() != 0 {
            let e = tag_iter.errors.join(", ");
            println!("RUSTDOC ERROR: {e}");
        }
        println!("RESULT: {model_tokens:#?}");
    }
}
