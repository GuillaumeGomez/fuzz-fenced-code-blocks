eBNF and fuzz checker for rustdoc lang strings
==============================================

This repository contains the lang string parser from rustdoc [#110800], an
an eBNF, a `combine`-based model parser, and a fuzz checker that compares the
copy-and-pasted rustdoc parse with the `combine`-based one.

To test the model, run `cargo run 'rust { .myClass }'` or `cargo test`

```text
$ cargo run --release '"!"'
    Finished release [optimized] target(s) in 0.01s
     Running `target/release/fuzz-fenced-code-blocks '"'\!'"'`
RESULT: [
    LangToken(
        "!",
    ),
]
$ cargo run --release '!'
    Finished release [optimized] target(s) in 0.01s
     Running `target/release/fuzz-fenced-code-blocks ''\!''`
RUSTDOC parser passed, while MODEL parser errored: ERROR: Parse error at PointerOffset(0x558950e58be0)
Unexpected `!`
Expected `{`, `"` or <bareword>

MODEL: []
RUSTDOC: [
    LangToken(
        "!",
    ),
]
$ cargo test
    Finished test [optimized + debuginfo] target(s) in 0.01s
     Running unittests src/main.rs (target/debug/deps/fuzz_fenced_code_blocks-2e6ad856bcf5c241)

running 3 tests
test tests::hardcoded_failing_tests ... ok
test quickcheck_tests::equiv ... FAILED
test tests::hardcoded_passing_tests ... ok
```

To run the fuzz test, run `cargo test`

```ebnf
lang-string = *(token-list / delimited-attribute-list / comment)

bareword = CHAR *(CHAR)
quoted-string = QUOTE *(NONQUOTE) QUOTE
token = bareword / quoted-string
sep = COMMA/WS *(COMMA/WS)
attribute = (DOT token)/(token EQUAL token)
attribute-list = [sep] attribute *(sep attribute) [sep]
delimited-attribute-list = OPEN-CURLY-BRACKET attribute-list CLOSE-CURLY-BRACKET
token-list = [sep] token *(sep token) [sep]
comment = OPEN_PAREN *(all characters) CLOSE_PAREN

OPEN-CURLY-BRACKET = "{"
CLOSE-CURLY-BRACKET = "}"
OPEN_PAREN = "("
CLOSE_PARENT = ")"
CHAR = ALPHA / DIGIT / "_" / "-" / ":"
QUOTE = %x22
NONQUOTE = %x09 / %x20 / %x21 / %x23-7E ; TAB / SPACE / all printable characters except `"`
COMMA = ","
DOT = "."
EQUAL = "="

ALPHA = %x41-5A / %x61-7A ; A-Z / a-z
DIGIT = %x30-39
WS = %x09 / " "
```
